package com.example.mobilesystems_1;

import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button buttonGreen = (Button) findViewById(R.id.buttonGreen);
        Button buttonYellow = (Button) findViewById(R.id.buttonYellow);
        Button buttonRed = (Button) findViewById(R.id.buttonRed);

        final ConstraintLayout myLayout = (ConstraintLayout) findViewById(R.id.myLayout);

        buttonYellow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //mInfoTextView.setText(R.string.yellow);
                myLayout.setBackgroundColor(getResources().getColor(R.color.yellow));
            }
        });

        buttonGreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //mInfoTextView.setText(R.string.yellow);
                myLayout.setBackgroundColor(getResources().getColor(R.color.green));
            }
        });

        buttonRed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //mInfoTextView.setText(R.string.yellow);
                myLayout.setBackgroundColor(getResources().getColor(R.color.red));
            }
        });
    }
}
